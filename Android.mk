# Copyright 2012 The Android Open Source Project

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= atrace.cpp

LOCAL_C_INCLUDES += external/zlib

LOCAL_MODULE:= atrace

LOCAL_MODULE_TAGS:= optional

# xzl 
LOCAL_FORCE_STATIC_EXECUTABLE:= true

# xzl
LOCAL_STATIC_LIBRARIES :=\
    libbinder \
    libcutils \
    libutils \
    libz \
    libc \
    liblog \

#LOCAL_SHARED_LIBRARIES := \


include $(BUILD_EXECUTABLE)
