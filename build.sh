#!/bin/bash

#
# builds atrace from source
#

pushd .

cd ~/aosp/
source build/envsetup.sh
lunch 1
mmm frameworks/native/cmds/atrace -B

popd

# grab
scp office:/home/xzl/aosp/out/target/product/generic/obj/EXECUTABLES/atrace_intermediates/LINKED/atrace /tmp/
# publish
scp office:/home/xzl/aosp/out/target/product/generic/obj/EXECUTABLES/atrace_intermediates/LINKED/atrace k2:/ftp/xzl/inv/bin/

adb push /tmp/atrace /sdcard/

echo 
echo 
echo 
echo "---------- next step ---------------"
echo "# on device"
echo "su"
echo "mv /sdcard/atrace /data/"
echo "chmod 777 /data/atrace"
echo "/data/atrace -h"
